package com.oracle.microservices.pollingservice.Proxy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "dummy-service")
@Component
public interface ServiceProxy {

    @GetMapping("/getName")
    public String getName();


}
