package com.oracle.microservices.pollingservice.Messaging;

import com.oracle.microservices.pollingservice.Proxy.ServiceProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

@Component
@EnableBinding({Sink.class})
public class EventAggregrator {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ServiceProxy serviceProxy;


    @StreamListener(Sink.INPUT)
    public void pollRowData(RowData rowData){

        logger.info("row data recieved -->"+ rowData);

        //calling other service
        serviceProxy.getName();

    }
}
