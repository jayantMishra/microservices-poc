package com.oracle.microservices.insertionservice.Messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class SimpleSourceBean {

    private Source source;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public SimpleSourceBean(Source source){
        this.source = source;
    }

    public void publishRowData(String url, String org_id){

        RowData rowData = new RowData(url, org_id);

        source.output().send(MessageBuilder.withPayload(rowData).build());
    }
}
