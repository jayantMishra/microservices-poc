package com.oracle.microservices.insertionservice.Controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.oracle.microservices.insertionservice.Messaging.SimpleSourceBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InsertionController {

    @Autowired
    SimpleSourceBean simpleSourceBean;

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @GetMapping("/insertRow")
    @HystrixCommand(fallbackMethod = "insertRowSync")
    public void inserRowAsync() {

        String url = "https";

        simpleSourceBean.publishRowData(url, "oracle");

    }

    public void insertRowSync() {

        // to add synchronous approach here

    }

}
