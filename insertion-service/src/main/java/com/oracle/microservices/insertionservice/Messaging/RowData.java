package com.oracle.microservices.insertionservice.Messaging;


public class RowData {

    private String url;

    private String org_id;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public RowData(String url, String org_id) {
        this.url = url;
        this.org_id = org_id;
    }
}
