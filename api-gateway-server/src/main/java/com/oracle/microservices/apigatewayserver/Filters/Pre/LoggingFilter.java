package com.oracle.microservices.apigatewayserver.Filters.Pre;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author jayant mishra
 * @version 1.0
 *
 * This filter logs the inbound request uri, executes before all the other filer,
 */

@Component
public class LoggingFilter extends ZuulFilter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {

        //Givign highest priority to this filter
        return 6;
    }

    @Override
    public boolean shouldFilter() {

        // running this filter for every request
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        HttpServletRequest request =  RequestContext.getCurrentContext().getRequest();

        logger.info("request is -->  {} Request uri ---> {}", request, request.getRequestURI());

        return null;
    }
}
