package com.oracle.microservices.dummyservice.Controller;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    private  Logger logger = LoggerFactory.getLogger(this.getClass());


    @GetMapping("/getName")
    @HystrixCommand(fallbackMethod = "defaultName")
    public String getName(){

        logger.info("inside get name");

        return "dummy";
    }

    public String defaultName() {

        logger.info("inside default name");
        return "defaultName";
    }

}
