## Ports

    |     Application       |     Port          |
    | ------------- | ------------- |
    | Registry-Discovery Server | 8761 |
    | API Gateway Server | 8765 |
    | Zipkin Distributed Tracing Server | 9411 |
    | Dummy Server | 8100, 8101.... |
    | Insertion Service | 8200, 8201 |
    | Polling Service | 8300 , 8301 .. |