package com.oracle.microservices.configurationsserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


/**
 * @author jaymishr
 *
 * This class acts as a centralized config server for other services properties,
 * currently other services configurations are stored in classpath/ respurces/sharedConfigurations directory
 */

@SpringBootApplication
@EnableConfigServer
public class ConfigurationsServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigurationsServerApplication.class, args);
    }
}
